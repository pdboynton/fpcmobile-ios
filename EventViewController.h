//
//  EventViewController.h
//  Copyright (c) 2012 Peter Boynton
//

#import <UIKit/UIKit.h>
#import "MWFeedParser.h"

@interface EventViewController : UITableViewController <MWFeedParserDelegate> {
	
	// Parsing
	MWFeedParser *feedParser;
	NSMutableArray *parsedItems;
	
	// Displaying
	NSArray *itemsToDisplay;
	NSDateFormatter *formatter;
	
}

// Properties
@property (nonatomic, retain) NSArray *itemsToDisplay;

@end

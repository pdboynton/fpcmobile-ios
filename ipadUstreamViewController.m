//
//  SecondViewController.m
//  FPCMobile
//
//  Created by Peter Boynton on 8/25/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ipadUstreamViewController.h"


@implementation ipadUstreamViewController

-(void)awakeFromNib
{
    // Test Flight Checkpoint
    [TestFlight passCheckpoint:@"Load_ipadUstream"];
    
	[ustream loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://ustream.tv/embed/10786920"]]];
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad. 
// Ustream test links: http://ustream.tv/embed/10786920 http://iphone-streaming.ustream.tv/ustreamVideo/6540154/streams/live/playlist.m3u8 http://iphone-streaming.ustream.tv/ustreamVideo/10786920/streams/live/playlist.m3u8  10786920
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end

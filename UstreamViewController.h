//
//  SecondViewController.h
//  FPCMobile
//
//  Created by Peter Boynton on 8/25/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h> 


@interface UstreamViewController : UIViewController {
 	IBOutlet UIWebView *ustream;   
}
@property (nonatomic, retain) IBOutlet UstreamViewController *viewController;
@property (nonatomic, retain) IBOutlet UIWebView *ustream;

@end

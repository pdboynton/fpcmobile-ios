//
//  PSAboutScreenController.mm
//  FPCMobile
//
//  Created by Peter Boynton on 8/28/12.
//  Copyright 2012 First Pentecostal Church. All rights reserved.
//

#import "PSAboutScreenController.h"
#import "PSModuleController.h"


@implementation PSAboutScreenController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

+ (NSString*)generateAboutHTML
{

	//										

	NSString *body = [NSString stringWithFormat:
					  @"<div id=\"header\">\n\
					  <div class=\"title\">FPCMobile</div>\n\
					  <div class=\"version\"> Version %@</div>\n\
					  <center><i><a href=\"http://firstpentecostalchurch.com\">First Pentecostal Church</a></i><br />\n\
					  </div>\n\
					  <div id=\"main\">\n\
					  <p><b>Developed by: </b><br />\n\
					  Peter Boynton<br />\n\
					  and the FPC Media Ministries team\n\
					  </p>\n\
					  <p><b>With help from: </b><br />\n\
					  Michael Waterfall, \
					  Nic Carter, \
					  David Bell, \
					  Manfred Bergmann, \
					  Christoffer Björkskog, \
					  Jan Bubík, \
					  Vincenzo Carrubba, \
					  Cheree Lynley Designs, \
					  Dominique Corbex, \
					  Bruno Gätjens González, \
					  Grace Community Church (HK), \
					  Yiguang Hu, \
					  John Huss, \
					  Nakamaru Kunio, \
					  Vitaliy, \
					  Ian Wagner, \
					  Henko van de Weerd\
					  \n\
					  <br />\n\
					  &amp; all the FPC Mobile beta testers!\n\
					  </p>\n\
					  <p><b>Special thanks to: </b><br />\n\
					  Pastor Paul L. Brown <br />\n\
					  Tia Boynton\n\
					  </p>\n\
					  </div>\n\
					  <p>If you would like to see additional features in FPC Mobile or are willing to help develop it, please Email Us using the button in the top right corner &amp; we would love your help!</p>\
					  \
					  \n\
					  \n\
					  <p>FPCMobile uses several Open Source projects, including:<br />\n\
					  &bull; <i><a href=\"http://crosswire.org/pocketsword\">The PocketSword Project</a></i><br />\n\
					  &bull; <i><a href=\"http://www.crosswire.org/sword/index.jsp\">The SWORD Project</a></i><br />\n\
					  &bull; <i><a href=\"http://code.google.com/p/cocoahttpserver/\">CocoaHTTPServer</a></i><br />\n\
					  &bull; <i><a href=\"https://github.com/zbyhoo/EGOTableViewPullRefresh\">zbyhoo's fork of EGOTableViewPullRefresh</a></i><br />\n\
					  &bull; <i><a href=\"http://code.google.com/p/ziparchive/\">ZipArchive</a></i><br />\n\
					  </p>\
					  <br />\n\
					  <br />\n\
					  <br />&nbsp;<br />", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];//CFBundleShortVersionString
	
	
	
	
	return [NSString stringWithFormat: @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
			<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n\
			\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n\
			<html dir=\"ltr\" xmlns=\"http://www.w3.org/1999/xhtml\"\n\
			xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n\
			xsi:schemaLocation=\"http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\"\n\
			xml:lang=\"en\" >\n\
			<meta name='viewport' content='width=device-width' />\n\
			<head>\n\
			<style type=\"text/css\">\n\
			html {\n\
				-webkit-text-size-adjust: none; /* Never autoresize text */\n\
			}\n\
			body {\n\
				color: black;\n\
				background-color: white;\n\
				font-size: 11pt;\n\
				font-family: Helvetica;\n\
				line-height: 130%%;\n\
			}\n\
			#header {\n\
				font-weight: bold;\n\
				border-bottom: solid 1px gray;\n\
				padding: 5px;\n\
				background-color: #D5EEF9;\n\
			}\n\
			#main {\n\
				padding: 10px;\n\
				text-align: center;\n\
			}\n\
			div.version {\n\
				font-size: 9pt;\n\
				text-align: center;\n\
			}\n\
			div.title {\n\
				font-size: 14pt;\n\
				text-align: center;\n\
			}\n\
			i {\n\
				font-size: 9pt;\n\
				font-weight: lighter;\n\
			}\n\
			div.crosswire {\n\
				font-size: 9pt;\n\
				font-weight: lighter;\n\
			}\n\
			h2.headbar {\n\
				background-color : #660000;\n\
				color : #dddddd;\n\
				font-weight : bold;\n\
				font-size:1em;\n\
				padding-left:1em;\n\
			}\n\
			</style>\n\
			</head>\n\
			<body><div>%@</div></body></html>", 
			body];
}

-(IBAction)launchFeedback {
    [TestFlight openFeedbackView];
}


- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	self.navigationItem.title = NSLocalizedString(@"AboutTitle", @"About");
	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
	[aboutWebView loadHTMLString:[PSAboutScreenController generateAboutHTML] baseURL:nil];
	aboutWebView.delegate = self;
	
	self.navigationItem.rightBarButtonItem = nil;
	//if([MFMailComposeViewController canSendMail]) {
		//UIBarButtonItem *emailUsBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshDownloadSource:)];
		UIBarButtonItem *emailUsBarButtonItem = [[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(@"EmailUsButton", @"Email Us") style:UIBarButtonItemStyleBordered target:self action:@selector(emailFeedback:)];
		self.navigationItem.rightBarButtonItem = emailUsBarButtonItem;
		[emailUsBarButtonItem release];
	//} else {
		//what should I show here instead?
	//}

	
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	self.navigationItem.rightBarButtonItem = nil;
}

-(void)emailFeedback:(id)sender
{
    NSString *recipients = @"av@firstpentecostalchurch.com";
	
	NSString *subject = [NSString stringWithFormat:@"FPCMobile Feedback (v%@ - %@ %@ (%@))", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"], [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion], [[UIDevice currentDevice] model]];
	
	if([MFMailComposeViewController canSendMail]) {
		MFMailComposeViewController *mailComposeViewController = [[MFMailComposeViewController alloc] init];
		[mailComposeViewController setSubject:subject];
		[mailComposeViewController setToRecipients:[NSArray arrayWithObject:recipients]];
		mailComposeViewController.mailComposeDelegate = self;
		mailComposeViewController.navigationBar.barStyle = UIBarStyleBlack;
		[self.tabBarController presentModalViewController:mailComposeViewController animated:YES];
		[mailComposeViewController release];
		//NSString *email = [NSString stringWithFormat:@"mailto:%@?subject=%@", recipients, subject];
		 //email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		 //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
	}
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	[self.tabBarController dismissModalViewControllerAnimated:YES];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	if(navigationType == UIWebViewNavigationTypeLinkClicked) {
		[[UIApplication sharedApplication] openURL:[request URL]];
		return NO;
	}
	return YES;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [super dealloc];
}


@end

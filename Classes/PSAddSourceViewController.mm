//
//  PSAddSourceViewController.m
//  FPCMobile
//
//  Created by Nic Carter on 10/06/10.
//  Copyright 2010 CrossWire Bible Society. All rights reserved.
//

#import "PSAddSourceViewController.h"
#import "NavigatorSources.h"
#import "PSModuleController.h"


@implementation PSAddSourceViewController

@synthesize serverType;

#define CAPTION_SECTION	0
#define SERVER_SECTION	1
#define PATH_SECTION	2
#define SECTIONS		3


#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
	
    CGRect fieldFrames = CGRectMake(20,12,280,25);
    if([PSResizing iPad]) {
        //different frames for the iPad
        fieldFrames = CGRectMake(60,12,560,25);
    }

    captionTextField = [[UITextField alloc] initWithFrame:fieldFrames];
	[captionTextField setPlaceholder:@"e.g. CrossWire 1"];
	captionTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	captionTextField.autocorrectionType = UITextAutocorrectionTypeNo;
	captionTextField.returnKeyType = UIReturnKeyNext;
	captionTextField.delegate = self;
	
	serverTextField = [[UITextField alloc] initWithFrame:fieldFrames];
	[serverTextField setPlaceholder:@"e.g. ftp.crosswire.org"];
	serverTextField.keyboardType = UIKeyboardTypeURL;
	serverTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	serverTextField.autocorrectionType = UITextAutocorrectionTypeNo;
	serverTextField.returnKeyType = UIReturnKeyNext;
	serverTextField.delegate = self;
	
	pathTextField = [[UITextField alloc] initWithFrame:fieldFrames];
	[pathTextField setPlaceholder:@"e.g. /pub/sword/raw"];
	pathTextField.keyboardType = UIKeyboardTypeURL;
	pathTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	pathTextField.autocorrectionType = UITextAutocorrectionTypeNo;
	pathTextField.returnKeyType = UIReturnKeyDone;
	pathTextField.delegate = self;	

}

- (void)viewWillAppear:(BOOL)animated {
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc addObserver:self selector:@selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object:nil];
	[nc addObserver:self selector:@selector(keyboardDidShow:) name: UIKeyboardDidShowNotification object:nil];
	NSString *t = [NSString stringWithFormat:@"Add%@SourceTitle", serverType];
	navBar.title = NSLocalizedString(t, @"");
	[captionTextField becomeFirstResponder];
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc removeObserver:self];
	[super viewWillDisappear:animated];
}

- (void)keyboardWillShow:(NSNotification *)note {
    if([PSResizing iPad]) {
        //don't do this magic on the iPad
        return;
    }
    CGRect r  = addSourceTableView.frame, t;
    [[note.userInfo valueForKey:UIKeyboardBoundsUserInfoKey] getValue: &t];//use UIKeyboardFrameEndUserInfoKey in iOS4
    //[[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &t];
	//UIWindow* mainWindow = (((FPCMobileAppDelegate*) [UIApplication sharedApplication].delegate).window);
	//t = [mainWindow convertRect:t fromWindow:nil];
    ////r.size.height -=  t.size.height;
    r.size.height = 416 - t.size.height;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    addSourceTableView.frame = r;
	[UIView commitAnimations];
}

- (void)keyboardDidShow:(NSNotification *)note {
	if([captionTextField isFirstResponder]) {
		[addSourceTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:CAPTION_SECTION] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
	} else if([serverTextField isFirstResponder]) {
		[addSourceTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:SERVER_SECTION] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
	} else if([pathTextField isFirstResponder]) {
		[addSourceTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:PATH_SECTION] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if([captionTextField isFirstResponder]) {
		[serverTextField becomeFirstResponder];
		[addSourceTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:SERVER_SECTION] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
	} else if([serverTextField isFirstResponder]) {
		[pathTextField becomeFirstResponder];
		[addSourceTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:PATH_SECTION] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
	} else if([pathTextField isFirstResponder]) {
		[self saveButtonPressed];
	}
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	[self keyboardDidShow:nil];
}
	   
- (void)keyboardWillHide:(NSNotification *)note {
    if([PSResizing iPad]) {
        //don't do this magic on the iPad
        return;
    }
    CGRect r  = addSourceTableView.frame;
	//CGRect t;
    //[[note.userInfo valueForKey:UIKeyboardBoundsUserInfoKey] getValue: &t];
    //r.size.height +=  t.size.height;
	r.size.height = 416;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    addSourceTableView.frame = r;
	[UIView commitAnimations];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if (buttonIndex != [alertView cancelButtonIndex]) {
		[self addInstallSource:captionTextField.text withPath:pathTextField.text andServer:serverTextField.text];
	}
}

- (IBAction)cancelButtonPressed {
	[captionTextField resignFirstResponder];
	[serverTextField resignFirstResponder];
	[pathTextField resignFirstResponder];
	[navSources dismissModalViewControllerAnimated:YES];
}

- (IBAction)saveButtonPressed {
	NSString *caption = captionTextField.text;
	NSString *server = serverTextField.text;
	NSString *path = pathTextField.text;
	if(!caption || [caption isEqualToString:@""] || !server || [server isEqualToString:@""] || !path || [path isEqualToString:@""]) {
		//you must fill in all fields to add a new source
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error", @"") message: NSLocalizedString(@"FillInAllFieldsMessage", @"") delegate: self cancelButtonTitle: NSLocalizedString(@"Ok", @"Ok") otherButtonTitles: nil];
		[alertView show];
		[alertView release];
		return;
	}

	if(![PSModuleController checkNetworkConnection]) {
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error", @"") message: NSLocalizedString(@"NoNetworkConnection", @"No network connection available.") delegate: self cancelButtonTitle: NSLocalizedString(@"Ok", @"") otherButtonTitles: nil];
		[alertView show];
		[alertView release];
		return;
	}
	
	
	UIApplication *application = [UIApplication sharedApplication];
	application.networkActivityIndicatorVisible = YES;
	
	[[NSNotificationCenter defaultCenter] postNotificationName:NotificationDisplayBusyIndicator object:nil];

	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@://%@%@/mods.d.tar.gz", [serverType lowercaseString], server, path]];
	NSData *data = nil;

	NSURLResponse *response = [[[NSURLResponse alloc] init] autorelease]; 
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:NotificationHideBusyIndicator object:nil];

	application.networkActivityIndicatorVisible = NO;

	if(!data) {
		//perhaps dodgy, display a warning.
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Warning", @"") message: NSLocalizedString(@"CannotVerifyInstallSourceWarning", @"") delegate: self cancelButtonTitle: NSLocalizedString(@"No", @"No") otherButtonTitles: NSLocalizedString(@"Yes", @"Yes"), nil];
		[alertView show];
		[alertView release];
		return;
	} else {
	}
	
	[self addInstallSource:caption withPath:path andServer:server];
}

- (void)addInstallSource:(NSString*)caption withPath:(NSString*)path andServer:(NSString*)server {
	SwordInstallSource *is = [[SwordInstallSource alloc] initWithType:serverType];
	
	[is setCaption:caption];
	[is setDirectory:path];
	[is setSource:server];
	[is setUID:[NSString stringWithFormat:@"%@-%@", server, caption]];
	
	[[[PSModuleController defaultModuleController] swordInstallManager] addInstallSource:is];
	[is release];
	is = nil;
	[navSources resetTableSelection];
	
	[navSources dismissModalViewControllerAnimated:YES];
	captionTextField.text = @"";
	serverTextField.text = @"";
	pathTextField.text = @"";
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return SECTIONS;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
	switch(indexPath.section) {
		case CAPTION_SECTION:
			[cell addSubview:captionTextField];
			break;
		case SERVER_SECTION:
			[cell addSubview:serverTextField];
			break;
		case PATH_SECTION:
			[cell addSubview:pathTextField];
			break;
	}
	
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	switch(section) {
		case CAPTION_SECTION:
			return NSLocalizedString(@"AddSourceCaptionTitle", @"");
		case SERVER_SECTION:
			return NSLocalizedString(@"AddSourceServerTitle", @"");
		case PATH_SECTION:
			return NSLocalizedString(@"AddSourcePathTitle", @"");
	}
	return @"";
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
	[captionTextField release];
	[serverTextField release];
	[pathTextField release];
}


- (void)dealloc {
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if([PSResizing iPad]) {
        return [PSResizing shouldAutorotateToInterfaceOrientation:interfaceOrientation];
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
}

@end


//
//  PSBibleViewController.h
//  FPCMobile
//
//  Created by Nic Carter on 3/11/09.
//  Copyright 2009 The CrossWire Bible Society. All rights reserved.
//

#import "PSCommentaryViewController.h"

@interface PSBibleViewController : UIViewController <UIWebViewDelegate, UIActionSheetDelegate, PSWebViewDelegate> {

	IBOutlet UITabBarItem				*bibleTabBarItem;
	IBOutlet UIToolbar *bibleToolbar;
	
	IBOutlet PSWebView *webView;
	IBOutlet PSCommentaryViewController *commentaryView;
	
	IBOutlet id viewController;
	NSString *refToShow;
	NSString *jsToShow;
	NSString *tappedVerse;
	BOOL isFullScreen;
	UIView *previousTabBarView;
	BOOL finishedLoading;
}

@property (copy, readwrite) NSString *refToShow;
@property (copy, readwrite) NSString *jsToShow;
@property (copy, readwrite) NSString *tappedVerse;
@property (readonly) BOOL isFullScreen;

- (void)toggleFullscreen;
- (void)switchToFullscreen;
- (void)switchToNormalscreen;

@end

//
//  OfferingScreenController.mm
//  FPCMobile
//
//  Created by Peter Boynton on 8/28/12.
//  Copyright 2012 First Pentecostal Church. All rights reserved.
//

#import "OfferingScreenController.h"


@implementation OfferingScreenController



/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

+ (NSString*)generateOfferingHTML
{

	//

	NSString *body = [NSString stringWithFormat:
					  @"<div style=\"display:block;text-align:left\"><p><b><font size=\"3\">Partner With Us</font></b>\n\
                      <br />\n\
					  <p> Thank you for your interest in supporting the ministry. To answer the challenge of both demonstrating the word of God, and spreading the message of the Gospel, please use the button below:</p></div>\n\
                      <br />\n\
					  <center><a href=\"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=LNZ6FZG2JNLNA\" imageanchor=\"1\"><img border=\"0\" src=\"https://sites.google.com/a/firstpentecostalchurch.com/church/paypal-button2.png\"></a>\n\
                      "];
	
	
	
	
	return [NSString stringWithFormat: @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
			<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n\
			\"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n\
			<html dir=\"ltr\" xmlns=\"http://www.w3.org/1999/xhtml\"\n\
			xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n\
			xsi:schemaLocation=\"http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\"\n\
			xml:lang=\"en\" >\n\
			<meta name='viewport' content='width=device-width' />\n\
			<head>\n\
			<style type=\"text/css\">\n\
			html {\n\
				-webkit-text-size-adjust: none; /* Never autoresize text */\n\
			}\n\
			body {\n\
				color: black;\n\
				background-color: white;\n\
				font-size: 11pt;\n\
				font-family: Helvetica;\n\
				line-height: 130%%;\n\
			}\n\
			#header {\n\
				font-weight: bold;\n\
				border-bottom: solid 1px gray;\n\
				padding: 5px;\n\
				background-color: #D5EEF9;\n\
			}\n\
			#main {\n\
				padding: 10px;\n\
				text-align: center;\n\
			}\n\
			h2.headbar {\n\
				background-color : #660000;\n\
				color : #dddddd;\n\
				font-weight : bold;\n\
				font-size:1em;\n\
				padding-left:1em;\n\
			}\n\
			</style>\n\
			</head>\n\
			<body><div>%@</div></body></html>", 
			body];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	self.navigationItem.title = @"Offering";
	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
	[offeringWebView loadHTMLString:[OfferingScreenController generateOfferingHTML] baseURL:nil];
	offeringWebView.delegate = self;
    // Test Flight Checkpoint
    [TestFlight passCheckpoint:@"Load_iphoneoffering"];	
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}
// The following was replaced to allow the paypal link to open in mobile safari
//- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
//	if(navigationType == UIWebViewNavigationTypeLinkClicked) {
//		[[UIApplication sharedApplication] openURL:[request URL]];
//		return NO;
//	}
//	return YES;
//}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
	NSURL *url = [request URL];
    
	// Intercept the external http requests and forward to Safari.app
	// Otherwise forward to the PhoneGap WebView
    // Avoid opening iFrame URLs in Safari by inspecting the `navigationType`
	if ((navigationType == UIWebViewNavigationTypeLinkClicked && [[url scheme] isEqualToString:@"http"]) || [[url scheme] isEqualToString:@"https"]) {
		[[UIApplication sharedApplication] openURL:url];
		return NO;
	}
    return YES;
}





- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [super dealloc];
}


@end

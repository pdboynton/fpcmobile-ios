/*
	PocketSword - A frontend for viewing SWORD project modules on the iPhone and iPod Touch
	Copyright (C) 2008-2010 CrossWire Bible Society

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License along
	with this program; if not, write to the Free Software Foundation, Inc.,
	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#import "PSResizing.h"
#import "PSLaunchViewController.h"

@interface PocketSwordAppDelegate : NSObject <UIApplicationDelegate, PSLaunchDelegate> {
    IBOutlet UIWindow *window;
    IBOutlet UITabBarController *tabBarController;
	
	IBOutlet PSLaunchViewController *launchViewController;

	NSURL *urlToOpen;
	NSDictionary *launchedWithOptions;
	//IBOutlet id moduleManager;
	IBOutlet id viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
//@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;
@property (nonatomic, retain) NSURL *urlToOpen;
@property (nonatomic, retain) NSDictionary *launchedWithOptions;

+ (PocketSwordAppDelegate *)sharedAppDelegate;

@end

@interface UITabBarController (PocketSword)
@end

@interface UINavigationController (PocketSword)
@end
//
//  OfferingScreenController.h
//  FPCMobile
//
//  Created by Peter Boynton on 8/28/12.
//  Copyright 2012 First Pentecostal Church. All rights reserved.
//

#import <MessageUI/MessageUI.h>


@interface PadOfferingScreenController : UIViewController <UIWebViewDelegate> {
	IBOutlet UIWebView *padofferingWebView;
}
+ (NSString*)generatePadOfferingHTML;
@end
//
//  OfferingScreenController.h
//  FPCMobile
//
//  Created by Peter Boynton on 8/28/12.
//  Copyright 2012 First Pentecostal Church. All rights reserved.
//

#import <MessageUI/MessageUI.h>


@interface OfferingScreenController : UIViewController <UIWebViewDelegate> {
	IBOutlet UIWebView *offeringWebView;
}
+ (NSString*)generateOfferingHTML;
@end
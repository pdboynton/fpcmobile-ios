/*
	PocketSword - A frontend for viewing SWORD project modules on the iPhone and iPod Touch
	Copyright (C) 2008-2010 CrossWire Bible Society

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License along
	with this program; if not, write to the Free Software Foundation, Inc.,
	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#import "globals.h"

@interface HistoryController : UIViewController {

	ShownTab listType;

	IBOutlet UIBarButtonItem	*historyCloseButton;
	IBOutlet UIBarButtonItem	*historyClearButton;
	IBOutlet UINavigationItem	*historyNavigationItem;
	IBOutlet UITableView		*historyListTable;
	IBOutlet UINavigationBar	*historyNavigationBar;
}

- (void)setListType:(ShownTab)listType;
//- (ShownTab)listType;
- (IBAction)closeButtonPressed;

- (void)addBibleHistoryItem;
- (void)addCommentaryHistoryItem;

+ (void)addHistoryItem:(ShownTab)tabForHistory;
- (IBAction)trashButtonPressed:(id)sender;
- (void)removeHistoryItem:(NSInteger)historyIndex forTab:(ShownTab)tabForHistory;

@end

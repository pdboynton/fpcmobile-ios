    //
//  PSModuleUnlockViewController.mm
//  FPCMobile
//
//  Created by Nic Carter on 4/01/11.
//  Copyright 2011 CrossWire Bible Society. All rights reserved.
//

#import "PSModuleUnlockViewController.h"
#import "PSModuleController.h"


@implementation PSModuleUnlockViewController

#define UNLOCK_HELP_HTML @"<head>\n\
<meta name='viewport' content='width=device-width' />\n\
<style type=\"text/css\">\n\
body {\n\
color: white;\n\
background-color: black;\n\
font-size: 12pt;\n\
font-family: Helvetica;\n\
line-height: 130%%;\n\
}\n\
</style>\n\
</head>"



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	[unlockLabel setText:NSLocalizedString(@"ModuleEnterKeyTitle", @"Enter Key:")];
	unlockNavBarItem.title = NSLocalizedString(@"ModuleUnlockScreenTitle", @"Unlock Module");
	[unlockWebView loadHTMLString:@"<html><body bgcolor='black'>&nbsp;</body></html>" baseURL:nil];
	[unlockHelpWebView loadHTMLString:[NSString stringWithFormat:@"<html>%@<body>%@</body></html>", UNLOCK_HELP_HTML, NSLocalizedString(@"ModuleUnlockHelpText", @"")] baseURL:nil];
	unlockWebView.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc addObserver:self selector:@selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object:nil];
	[nc addObserver:self selector:@selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
	[unlockTextField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc removeObserver:self name:UIKeyboardWillHideNotification object:nil];
	[nc removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

- (IBAction)closeUnlockView:(id)sender {
	[self dismissModalViewControllerAnimated:YES];
}

- (IBAction)unlockSaveButtonPressed:(id)sender {
	//save the key
	[[[[PSModuleController defaultModuleController] swordManager] moduleWithName: infoNavItem.title] unlock: unlockTextField.text];
	//redisplay the text if this is the current primary bible/commentary
	if([infoNavItem.title isEqualToString:[[[PSModuleController defaultModuleController] primaryBible] name]]) {
		[[NSNotificationCenter defaultCenter] postNotificationName:NotificationRedisplayPrimaryBible object:nil];
	} else if([infoNavItem.title isEqualToString:[[[PSModuleController defaultModuleController] primaryCommentary] name]]) {
		[[NSNotificationCenter defaultCenter] postNotificationName:NotificationRedisplayPrimaryCommentary object:nil];
	}
	[self closeUnlockView:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	//load the key into the module & test it with:
	// @"Jeremiah 29:11"
	// @"Psalm 139:5"
	// @"John 3:16"
	unlockWebView.hidden = NO;
	NSMutableString *html = [NSMutableString string];
	SwordModule *mod = [[[PSModuleController defaultModuleController] swordManager] moduleWithName: infoNavItem.title];
	[mod unlock: unlockTextField.text];
	NSArray *refs = [NSArray arrayWithObjects:@"Jeremiah 29:11", @"Psalm 139:5", @"John 3:16", nil];
	for(NSString *ref in refs) {
		SwordModuleTextEntry *entry = [mod textEntryForKey:ref textType:TextTypeRendered];
		[html appendFormat:@"<p><b>%@:</b> %@</p>", [PSModuleController createRefString: entry.key], entry.text];
	}
	
	[unlockWebView loadHTMLString:[PSModuleController createHTMLString:html usingPreferences:YES withJS:@"" usingModuleForPreferences:mod.name] baseURL:nil];
	[mod unlock: nil];
	return YES;
}

- (IBAction)unlockEditButtonPressed:(id)sender {
	if([unlockTextField canBecomeFirstResponder])
		[unlockTextField becomeFirstResponder];
}

- (void)keyboardWillShow:(NSNotification *)note {
    CGRect r  = unlockToolbar.frame, t;
    [[note.userInfo valueForKey:UIKeyboardBoundsUserInfoKey] getValue: &t];//use UIKeyboardFrameEndUserInfoKey in iOS4
																		   //[[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &t];
																		   //UIWindow* mainWindow = (((PocketSwordAppDelegate*) [UIApplication sharedApplication].delegate).window);
																		   //t = [mainWindow convertRect:t fromWindow:nil];
    r.origin.y -=  t.size.height;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    unlockToolbar.frame = r;
	[UIView commitAnimations];
	[unlockEditButton setEnabled:NO];
	[unlockSaveButton setEnabled:NO];
}

- (void)keyboardWillHide:(NSNotification *)note {
    CGRect r  = unlockToolbar.frame, t;
    [[note.userInfo valueForKey:UIKeyboardBoundsUserInfoKey] getValue: &t];//use UIKeyboardFrameEndUserInfoKey in iOS4
																		   //[[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &t];
																		   //UIWindow* mainWindow = (((PocketSwordAppDelegate*) [UIApplication sharedApplication].delegate).window);
																		   //t = [mainWindow convertRect:t fromWindow:nil];
    r.origin.y +=  t.size.height;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    unlockToolbar.frame = r;
	[UIView commitAnimations];
	[unlockEditButton setEnabled:YES];
	[unlockSaveButton setEnabled:YES];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    if([PSResizing iPad]) {
        return [PSResizing shouldAutorotateToInterfaceOrientation:interfaceOrientation];
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end

//
//  SecondViewController.m
//  FPCMobile
//
//  Created by Peter Boynton on 8/25/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "UstreamViewController.h"

@implementation UstreamViewController
@synthesize viewController;
@synthesize ustream;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Test Flight Checkpoint
    [TestFlight passCheckpoint:@"Load_iphoneUstream"];
    
    //Load the request in the UIWebView.
//    NSURL *url = [NSURL URLWithString:@"http://www.ustream.tv/socialstream/10786920"];
//    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//    [ustream loadRequest:requestObj];
    [ustream loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://ustream.tv/embed/10786920"]]];}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad. 
// Links used to test ustream: http://ustream.tv/embed/10786920 http://iphone-streaming.ustream.tv/ustreamVideo/6540154/streams/live/playlist.m3u8 http://iphone-streaming.ustream.tv/ustreamVideo/10786920/streams/live/playlist.m3u8  http://www.ustream.tv/socialstream/10786920 10786920

// Below removed in favor of the above method via ustream directly
//-(IBAction)playMovie:(id)sender
//{
    // Test Flight Checkpoint
//    [TestFlight passCheckpoint:@"Press_iphoneUstreamlive"];
    
//    NSURL *url = [NSURL URLWithString:@"http://iphone-streaming.ustream.tv/ustreamVideo/10786920/streams/live/playlist.m3u8"];
//    MPMoviePlayerController *moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:url];
	
//    [[NSNotificationCenter defaultCenter] addObserver:self
//											 selector:@selector(moviePlaybackComplete:)
//												 name:MPMoviePlayerPlaybackDidFinishNotification
//											   object:moviePlayerController];
	
//	[self.view addSubview:moviePlayerController.view];
//    moviePlayerController.fullscreen = YES;
	
//	moviePlayerController.scalingMode = MPMovieScalingModeAspectFit;
	
//    [moviePlayerController play];
//}

- (void)moviePlaybackComplete:(NSNotification *)notification
{
    MPMoviePlayerController *moviePlayerController = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
													name:MPMoviePlayerPlaybackDidFinishNotification
												  object:moviePlayerController];
	
    [moviePlayerController.view removeFromSuperview];
    [moviePlayerController release];
}

- (void)dealloc {
    [super dealloc];
}

/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


//- (void)dealloc {
//    [super dealloc];
//}


@end
